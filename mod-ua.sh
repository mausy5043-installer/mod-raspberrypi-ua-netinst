#! /bin/bash

CLOPT=("$@")
WIFI=false
CLIENT="raspberrypi"
NETINST="../raspberrypi-ua-netinst"
BRANCH="../pi-netinst.branch"
WPA="../wpa.conf"
DOUPDATE=true

echo ""
echo "**************************************************"
# shellcheck disable=SC2145
echo "Arguments passed to mod-ua.sh: ${CLOPT[@]}"
echo ""

while true; do
  case "$1" in
    -w | --wifi ) WIFI=true; shift ;;
    -n | --name ) CLIENT="$2"; shift; shift ;;
    -u | --noupdate )  DOUPDATE=false; echo "Not re-updating"; shift ;;
    -- ) shift; break ;;
    * ) shift ;;
  esac
done

# Check if the `raspberrypi-ua-netinst` directory is present.
if [ ! -d $NETINST ]; then
  echo "!!! A clone of raspberrypi-ua-netinst could not be found." >&2
  exit 1
fi

# Check if `pi-netinst.branch` file exists
# This file contains the name of the branch that should be used.
if [ ! -e $BRANCH ]; then
  echo ""
  echo "!!! Could not find $BRANCH" >&2
  echo "    This file should contain the name of the branch to be used."
  echo "    Both the branchname of raspberrypi-ua-netinst and the branchname of mod-raspberrypi-ua-netinst must be the same."
  exit 1
fi
BRANCH=$(cat $BRANCH)

echo "Settings being used:" | tee >(logger -t mod-raspberry)
echo "Wi-fi=$WIFI" | tee >(logger -t mod-raspberry)
echo "Name=$CLIENT" | tee >(logger -t mod-raspberry)
echo "Branch=$BRANCH" | tee >(logger -t mod-raspberry)
echo "**************************************************"
echo ""
echo ""
echo ""
echo ""
echo "**************************************************"
echo "*** Updating the RASPBERRYPI-UA-NETINST files ****" | tee >(logger -t mod-raspberry)
echo "**************************************************"
echo ""
pushd $NETINST/ || exit 1
  git pull
  git fetch origin
  git checkout "$BRANCH"
  git reset --hard "origin/$BRANCH" && \
  git clean -f -d
popd || exit 1

echo ""
echo ""
echo "**************************************************"
echo "*** Putting modifications in place ***************" | tee >(logger -t mod-raspberry)
echo "*** Copying overlayed files..."
cp -rv ./overlay/* $NETINST/
if [ "$WIFI" == true ]; then
  echo "*** Copying WIFI settings..."
  echo "ifname=wlan0"           >> $NETINST/config/installer-config.txt
  # echo "drivers_to_load=8192cu" >> $NETINST/config/installer-config.txt
  cp -rv $WPA $NETINST/config/wpa_supplicant.conf
fi

echo "*** Fixing install.sh for presence of group netdev..."
orgfile="${NETINST}/scripts/opt/raspberrypi-ua-netinst/install.sh"
tmpfile="${NETINST}/scripts/opt/raspberrypi-ua-netinst/install.sh.bak1"
cp ${orgfile} ${tmpfile}
awk 'NR==875{print "echo 'netdev:x:111:' >>/etc/group"}1' ${tmpfile} > ${orgfile}
# awk 'NR==875{print "addgroup -S netdev"}1' ${tmpfile} > ${orgfile}

#echo "*** Using bullseye..."
#fnd="release=buster"
#rpl="release=bullseye"
#sed -ibak1 "s/${fnd}/${rpl}/" $NETINST/update.sh

# echo "*** Fixing renamed hwmon.ko..."
# fnd="\/hwmon\.ko"
# rpl="\/raspberrypi-hwmon\.ko"
# sed -ibak2 "s/${fnd}/${rpl}/" $NETINST/build.sh

# echo "*** Fixing copy errors..."
# # shellcheck disable=SC2016
# fnd='\"${2\/\/kernel\*\/${kernel}}\"'
# # shellcheck disable=SC2016
# rpl='\"${2\/\/kernel\*\/${kernel}}\" || true'
# sed -ibak3 "s#${fnd}#${rpl}#" $NETINST/build.sh

# echo "*** Fixing mkfs.btrfs location..."
# fnd="tmp\/bin\/mkfs.btrfs rootfs\/bin\/"
# rpl="tmp\/sbin\/mkfs.btrfs rootfs\/sbin\/"
# sed -ibak3 "s/${fnd}/${rpl}/" $NETINST/build.sh

echo "**************************************************"
echo ""
echo ""
echo ""
echo ""
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@ Building RASPBERRYPI-UA-NETINST image @@@@@@@@" | tee >(logger -t mod-raspberry)
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo ""
pushd $NETINST/ || exit 1
  if $DOUPDATE; then
    # change the hostname in the default installer-config.txt
    sed -ibak "s/raspberrypi/${CLIENT}/" ./config/installer-config.txt
    echo ""
    echo ""
    echo ""
    echo "**************************************************"
    echo "*** Cleaning the installer ***********************" | tee >(logger -t mod-raspberry)
    echo "**************************************************"
    echo ""
    ./clean.sh || exit 1

    echo ""
    echo ""
    echo ""
    echo "**************************************************"
    echo "*** Updating the installer packages **************" | tee >(logger -t mod-raspberry)
    echo "**************************************************"
    echo ""
    ./update.sh || exit 1
    else
      echo "SKIPPING INSTALLER UPDATE"
    fi

  echo ""
  echo ""
  echo ""
  echo "**************************************************"
  echo "*** Building the installer ***********************" | tee >(logger -t mod-raspberry)
  echo "**************************************************"
  echo ""
  # We don't need the zip file
  sed -i 's/cd bootfs && zip/#not zipping#/' ./build.sh
  sed -i '/#not zipping#/{n;s/.*/#############/}' ./build.sh
  ./build.sh || exit 1
  # By default don't `./buildroot`
  #./buildroot.sh

  # At the end we don't `./clean.sh` so we can use the files in `./bootfs/` directly.
popd || exit 1
