#! /bin/busybox sh
# post-install.txt
# finish set-up of basic server


#####################
#### PREPARATION ####
#####################

# This is where we setup variables and stuff that we need later in the script

# shellcheck disable=SC2154
USER=$username
USRHOME="home/"$USER
RMTSERVER="bifrost.fritz.box"
RMTSERVER2="rbfile.fritz.box"


######################
#### SYSTEM SETUP ####
######################

# Here we setup the generic system. Stuff that's needed to get a basic RPI up
# and running stable.

# For the paranoid:
echo ""
echo "Regenerating all the host SSH keys..."
echo""
rm /rootfs/etc/ssh/ssh_host_*
chroot /rootfs /usr/bin/ssh-keygen -N "" -t rsa   -b 4096 -f /etc/ssh/ssh_host_rsa_key
chroot /rootfs /usr/bin/ssh-keygen -N "" -t dsa   -b 1024 -f /etc/ssh/ssh_host_dsa_key
chroot /rootfs /usr/bin/ssh-keygen -N "" -t ecdsa -b 521  -f /etc/ssh/ssh_host_ecdsa_key
chroot /rootfs /usr/bin/ssh-keygen -N "" -t ed25519       -f /etc/ssh/ssh_host_ed25519_key

echo ""
echo "Preparing /etc/hosts file..."
echo ""
{
  echo "# Added by post-install:"
  echo "::1             localhost ip6-localhost ip6-loopback"
  echo "fe00::0         ip6-localnet"
  echo "ff00::0         ip6-mcastprefix"
  echo "ff02::1         ip6-allnodes"
  echo "ff02::2         ip6-allrouters"
}>> /rootfs/etc/hosts

echo ""
echo "Creating extra mountpoints..."
echo ""

mkdir -p /rootfs/srv/config
mkdir -p /rootfs/srv/databases
mkdir -p /rootfs/srv/files

echo ""
echo "Updating /etc/fstab ..."
echo ""
# Disable default /tmp mount
sed -i "s/tmpfs \/tmp/#&/" /rootfs/etc/fstab
{
  echo ""
  echo "# Added by post-install:"
  echo "tmpfs                            /tmp           tmpfs    defaults,nodev,nosuid,size=80%                                    0   0"
  echo "# NFS fileserver mountpoints"
  echo "${RMTSERVER2}:/srv/nfs/config     /srv/config     nfs4     nouser,atime,rw,dev,exec,suid,_netdev,x-systemd.automount,noauto  0   0"
  echo "${RMTSERVER2}:/srv/nfs/databases  /srv/databases  nfs4     nouser,atime,rw,dev,exec,suid,_netdev,x-systemd.automount,noauto  0   0"
  echo "${RMTSERVER2}:/srv/nfs/files      /srv/files      nfs4     nouser,atime,rw,dev,exec,suid,_netdev,x-systemd.automount,noauto  0   0"
  # legacy mountpoints:
  # echo "${RMTSERVER}:/srv/nfs/sqlite3     /srv/data       nfs4     nouser,atime,rw,dev,exec,suid,_netdev,x-systemd.automount,noauto  0   0"
  # echo "${RMTSERVER}:/srv/nfs/config      /srv/config    nfs4     nouser,atime,rw,dev,exec,suid,_netdev,x-systemd.automount,noauto  0   0"
} >> /rootfs/etc/fstab

echo ""
echo "Updating Locale information..."
echo ""
chroot /rootfs /usr/sbin/update-locale LC_ALL="en_US.UTF-8"
chroot /rootfs /usr/sbin/update-locale LANGUAGE="en_US:en"

echo "Updating apt..."
echo ""
eval chroot /rootfs /usr/bin/apt-get -y update | output_filter

# echo ""
# echo "Removing rsyslog..."
# echo ""
# eval chroot /rootfs /usr/bin/apt-get -y purge rsyslog 2>&1 | output_filter

# echo ""
# echo "Installing syslog-ng..."
# echo ""
# eval chroot /rootfs /usr/bin/apt-get -y install syslog-ng 2>&1 | output_filter

####################
#### USER SETUP ####
####################

# Here we setup the generic user `pi` and the luser environment.

# SSH
echo ""
if [ ! -d "/rootfs/${USRHOME}/.ssh" ]; then
  echo "Creating ~/.ssh folder..."
  mkdir -m 0700 -p "/rootfs/${USRHOME}/.ssh"
fi

# GIT
echo ""
echo "Set-up raspboot..."
echo ""
mkdir -p "/rootfs/${USRHOME}/raspboot"
echo "master" > "/rootfs/${USRHOME}/.raspboot.branch"
chroot /rootfs /usr/bin/git clone -b master https://gitlab.com/mausy5043-installer/raspboot.git "/${USRHOME}/raspboot"
chmod -R 0755 "/rootfs/${USRHOME}/raspboot"

echo ""
echo "Set-up dotfiles..."
echo ""
mkdir -p "/rootfs/${USRHOME}/dotfiles"
chroot /rootfs /usr/bin/touch "${USRHOME}/.rsync"
chroot /rootfs /usr/bin/touch "${USRHOME}/.bin"
chroot /rootfs /usr/bin/git clone -b main https://gitlab.com/mausy5043/dotfiles.git "/${USRHOME}/dotfiles"
chmod -R 0755 "/rootfs/${USRHOME}/dotfiles"

echo ""
echo "Change ownership of files in $USRHOME..."
echo ""
chroot /rootfs /bin/chown -R "${USER}":"${USER}" "/${USRHOME}"


######################
#### SERVER SETUP ####
######################

# This is where specifics for the server are setup
# Install CPU governor
chroot /rootfs /usr/sbin/update-rc.d switch_cpu_governor defaults
echo "DNSSEC=no" >> "/rootfs/etc/systemd/resolved.conf"


####################
#### FINALISING ####
####################

echo -n "********** SYSTEM READY **********"
date
